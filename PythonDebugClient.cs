﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web.Script.Serialization;

namespace PythonDebugger
{
    public class PythonDebugClient
    {
        public Socket sock;

        //TODO: Set in constructor...
        byte[] bytes = new byte[1024];
        JavaScriptSerializer serializer = new JavaScriptSerializer();

        public bool StartClient(string filename)
        {
            bool result = false;

            // Connect to a remote device.  
            try
            {
                // Create a TCP/IP  socket.  
                sock = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);

                // Connect the socket to the remote endpoint. Catch any errors.  
                try
                {
                    //sock.Connect(remoteEP);
                    sock.Connect("127.0.0.1", 8080);

                    System.Diagnostics.Debug.Print("Socket connected to {0}",
                        sock.RemoteEndPoint.ToString());

                    //Send("{ \"request\": \"alive\" }");
                    Send(new PythonSocketRequest { request = "alive" });


                    PythonSocketRequest recv = Receive();
                    System.Diagnostics.Debug.Print("Echoed test = {0}", recv);
                    if (recv.response == "True")
                    {
                        result = true;
                        Send(new PythonSocketRequest { request = "debug", file = filename });
                    }
                }
                catch (ArgumentNullException ane)
                {
                    System.Diagnostics.Debug.Print("ArgumentNullException : {0}", ane.ToString());
                }
                catch (SocketException se)
                {
                    System.Diagnostics.Debug.Print("SocketException : {0}", se.ToString());
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.Print("Unexpected exception : {0}", e.ToString());
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Print(e.ToString());
            }

            return result;
        }

        //public void Send(string message)
        public void Send(object obj)
        {
            string message = serializer.Serialize(obj);

            // Encode the data string into a byte array.  
            byte[] msg = Encoding.ASCII.GetBytes(message);

            // Send the data through the socket.  
            int bytesSent = sock.Send(msg);
        }

        public PythonSocketRequest Receive()
        {
            // Receive the response from the remote device.  
            int bytesRec = sock.Receive(bytes);
            string message = Encoding.ASCII.GetString(bytes, 0, bytesRec);
            return serializer.Deserialize<PythonSocketRequest>(message);
        }

        public void StopClient()
        {
            // Release the socket.  
            sock.Shutdown(SocketShutdown.Both);
            sock.Close();
        }
    }
}
