import bdb
import socket
import json

class DbSocket:
    def __init__(self, host='127.0.0.1', port=8080):
        self.debugger = None
        self.cur_frame = None
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.bind((host, port))
        sock.listen(1)
        self.conn, self.addr = sock.accept()

    def send(self, msg):
        totalsent = 0
        while totalsent < len(msg):
            sent = self.conn.send(msg[totalsent:])
            if sent == 0:
                raise RuntimeError("socket connection broken")
            totalsent = totalsent + sent

    def receive(self):
        data = self.conn.recv(1024)
        if data == '':
            raise RuntimeError("socket connection broken")
        return data
    
    def close(self):
        self.conn.close()

    def process_data(self, data):
        if data['request'] == 'alive':
            data['response'] = True
        elif data['request'] == 'debug':
            filename = data['file']
            self.debugger = Sdb(socket=self)
            self.debugger.run_file(filename)
            #self.debugger.run_file(data['args']['file'])
            data['args'] = None
            data['response'] = 'complete'
        else:
            data['err'] = 'invalid request: ' + data['request']

        return data

    def begin_processing(self):
        data = { 'request': None }
        datastr = self.receive()
        while True:
            try:
                data = json.loads(datastr)
                if data['request'] == 'disconnect':
                    data['response'] = True
                    self.send(json.dumps(data))
                    break
                else:
                    response = self.process_data(data)
                    self.send(json.dumps(response))
            except:
                data = { 'request': None }
                self.send(json.dumps({ 'err': 'invalid json: ' + datastr}))

            datastr = self.receive()
        self.close()

class Sdb(bdb.Bdb):
    def __init__(self, skip=None, socket=None):
        bdb.Bdb.__init__(self, skip=skip)
        self.sock = socket

    def user_line(self, frame):
        print 'broke on line ' + str(frame.f_lineno)
        self.debug_request(frame)

    def debug_request(self, frame):
        print 'handling break in socket class...'
        print 'code: ' + str(frame.f_code)
        data = {
            'response': 'break',
            'line': frame.f_lineno,
            'code': str(frame.f_code)
        }
        try:
            temp = json.dumps(frame.f_locals)
            data['locals'] = frame.f_locals
        except:
            temp = json.dumps(frame.f_locals.keys())
            data['locals'] = frame.f_locals.keys()
        jsonstr = json.dumps(data)
        print jsonstr
        self.sock.send(jsonstr)

        while True:
            datastr = self.sock.receive()
            data = json.loads(datastr)

            #if 'locals' in data:
            #    try:
            #        jsonstr = json.dumps(frame.f_locals)
            #    except:
            #        jsonstr = json.dumps(frame.f_locals.keys())
            #    print 'locals: ' + jsonstr
            #    self.sock.send(jsonstr)
            #elif 'step' in data:
            if 'step' in data['request']:
                print 'stepping...'
                self.set_step()
                break
            elif 'next' in data['request']:
                print 'stepping over...'
                self.set_next(frame)
                break
            elif 'continue' in data['request']:
                print 'continuing...'
                self.set_continue()
                break
            elif 'stop' in data['request']:
                print 'stopping...'
                self.set_quit()
                break
            else:
                data['err'] = 'invalid request: ' + data['request']
                self.sock.send(json.dumps(data))


    def run_file(self, filename):
        import __main__
        #__main__.__dict__.clear()
        __main__.__dict__.update({"__name__"    : "__main__",
                                  "__file__"    : filename
                                 })
        self.run('execfile(%r)' % filename)

DbSocket().begin_processing()
