import time
import socket
import json
 
TCP_IP = '127.0.0.1'
TCP_PORT = 8080
BUFFER_SIZE = 1024
 
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))

#check if it's alive
print 'request: alive'
s.sendall(json.dumps({ 'request': 'alive'}))
data = json.loads(s.recv(BUFFER_SIZE))
print data

if data['response'] == True:
    print 'request: debug'
    s.sendall(json.dumps({ 'request': 'debug' }))
    datastr = s.recv(BUFFER_SIZE)
    #print datastr
   
    x = 15 
    while datastr != '' and datastr != None and x > 0:
        print 'iter: ' + str(x)
        time.sleep(1)

        print 'response data:'
        print datastr

        time.sleep(1)

        print 'request: step'
        s.sendall(json.dumps({ 'request': 'step' }))
        datastr = s.recv(BUFFER_SIZE)
        #print datastr

        x -= 1
    print 'request: disconnect'
    s.sendall(json.dumps({ 'request': 'disconnect' }))
    datastr = s.recv(BUFFER_SIZE)
    s.close()

print 'Done!'
 
