﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PythonDebugger
{
    public class PythonSocketRequest
    {
        public string request;
        public string file;
        public string response;
        public string err;
        public int line;
        public object locals;
        public string code;
    }
}
