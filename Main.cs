﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using NppPluginNET;

namespace PythonDebugger
{
    class Main
    {
        #region Fields

        internal const string PluginName = "PythonDebugger";
        static string iniFilePath = null;
        static bool someSetting = false;
        static frmPythonDebug frmConsole = null;
        static int idMyDlg = -1;
        static Bitmap tbBmp = Properties.Resources.star;
        static Bitmap tbBmp_tbTab = Properties.Resources.star_bmp;
        static Icon tbIcon = null;

        #endregion

        #region StartUp/CleanUp

        internal static void CommandMenuInit()
        {
            //Get and check INI Config file dir
            StringBuilder sbIniFilePath = new StringBuilder(Win32.MAX_PATH);
            Win32.SendMessage(PluginBase.nppData._nppHandle, NppMsg.NPPM_GETPLUGINSCONFIGDIR, Win32.MAX_PATH, sbIniFilePath);
            iniFilePath = sbIniFilePath.ToString();
            if (!Directory.Exists(iniFilePath))
                Directory.CreateDirectory(iniFilePath);

            iniFilePath = Path.Combine(iniFilePath, PluginName + ".ini");
            someSetting = (Win32.GetPrivateProfileInt("SomeSection", "SomeKey", 0, iniFilePath) != 0);

            PluginBase.SetCommand(0, "Show Python Debugger", myDockableDialog); idMyDlg = 1;
        }

        internal static void SetToolBarIcon()
        {
            toolbarIcons tbIcons = new toolbarIcons();
            tbIcons.hToolbarBmp = tbBmp.GetHbitmap();
            IntPtr pTbIcons = Marshal.AllocHGlobal(Marshal.SizeOf(tbIcons));
            Marshal.StructureToPtr(tbIcons, pTbIcons, false);
            Win32.SendMessage(PluginBase.nppData._nppHandle, NppMsg.NPPM_ADDTOOLBARICON, PluginBase._funcItems.Items[idMyDlg]._cmdID, pTbIcons);
            Marshal.FreeHGlobal(pTbIcons);
        }

        internal static void PluginCleanUp()
        {
            Win32.WritePrivateProfileString("SomeSection", "SomeKey", someSetting ? "1" : "0", iniFilePath);
        }

        #endregion

        #region Menu functions

        internal static void myDockableDialog()
        {
            if (frmConsole == null)
            {
                frmConsole = new frmPythonDebug();

                using (Bitmap newBmp = new Bitmap(16, 16))
                {
                    Graphics g = Graphics.FromImage(newBmp);
                    ColorMap[] colorMap = new ColorMap[1];
                    colorMap[0] = new ColorMap();
                    colorMap[0].OldColor = Color.Fuchsia;
                    colorMap[0].NewColor = Color.FromKnownColor(KnownColor.ButtonFace);
                    ImageAttributes attr = new ImageAttributes();
                    attr.SetRemapTable(colorMap);
                    g.DrawImage(tbBmp_tbTab, new Rectangle(0, 0, 16, 16), 0, 0, 16, 16, GraphicsUnit.Pixel, attr);
                    tbIcon = Icon.FromHandle(newBmp.GetHicon());
                }

                NppTbData nppTbData = new NppTbData {
                    hClient = frmConsole.Handle,
                    pszName = "Python Debugger",
                    dlgID = idMyDlg,
                    uMask = NppTbMsg.DWS_DF_CONT_RIGHT | NppTbMsg.DWS_ICONTAB | NppTbMsg.DWS_ICONBAR,
                    hIconTab = (uint)tbIcon.Handle,
                    pszModuleName = PluginName
                };
                IntPtr ptrNppTbData = Marshal.AllocHGlobal(Marshal.SizeOf(nppTbData));
                Marshal.StructureToPtr(nppTbData, ptrNppTbData, false);

                Win32.SendMessage(PluginBase.nppData._nppHandle, NppMsg.NPPM_DMMREGASDCKDLG, 0, ptrNppTbData);
            }
            else
            {
                Win32.SendMessage(PluginBase.nppData._nppHandle, NppMsg.NPPM_DMMSHOW, 0, frmConsole.Handle);
            }
        }

        #endregion

        #region Helper functions

        internal static string GetCurrentFilename()
        {
            StringBuilder sbFilePath = new StringBuilder(Win32.MAX_PATH);
            Win32.SendMessage(PluginBase.nppData._nppHandle, NppMsg.NPPM_GETFULLCURRENTPATH, Win32.MAX_PATH, sbFilePath);
            return sbFilePath.ToString();
        }

        internal static int DefineMarker(Color color)
        {
            IntPtr ptr = Win32.SendMessage(PluginBase.nppData._scintillaMainHandle, SciMsg.SCI_MARKERDEFINE, 1, (int)SciMsg.SC_MARK_BACKGROUND);

            int markerNumber = ptr.ToInt32();
            int colour = (color.R | color.G << 8 | color.B << 16);
            Win32.SendMessage(PluginBase.nppData._scintillaMainHandle, SciMsg.SCI_MARKERSETBACK, markerNumber, colour);

            return markerNumber;
        }

        internal static void DeleteMarker(int markerNumber)
        {
            Win32.SendMessage(PluginBase.nppData._scintillaMainHandle, SciMsg.SCI_MARKERDELETEHANDLE, markerNumber, 0);
        }

        internal static int AddMarkerToLine(int line, int markerNumber)
        {
            IntPtr ptr = Win32.SendMessage(PluginBase.nppData._scintillaMainHandle, SciMsg.SCI_MARKERADD, line, markerNumber);
            return ptr.ToInt32();
        }

        internal static void RemoveMarkerFromLine(int line, int markerNumber)
        {
            Win32.SendMessage(PluginBase.nppData._scintillaMainHandle, SciMsg.SCI_MARKERDELETE, line, markerNumber);
        }

        internal static void RemoveMarkerFromAllLines(int markerNumber)
        {
            Win32.SendMessage(PluginBase.nppData._scintillaMainHandle, SciMsg.SCI_MARKERDELETEALL, markerNumber, 0);
        }

        #endregion
    }
}