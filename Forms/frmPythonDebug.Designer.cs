﻿namespace PythonDebugger
{
    partial class frmPythonDebug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tvwLocals = new System.Windows.Forms.TreeView();
            this.btnBreak = new System.Windows.Forms.ToolStripButton();
            this.btnRun = new System.Windows.Forms.ToolStripButton();
            this.btnDebug = new System.Windows.Forms.ToolStripButton();
            this.btnStop = new System.Windows.Forms.ToolStripButton();
            this.btnStepIn = new System.Windows.Forms.ToolStripButton();
            this.btnStepOver = new System.Windows.Forms.ToolStripButton();
            this.btnStepOut = new System.Windows.Forms.ToolStripButton();
            this.btnContinue = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnBreak,
            this.toolStripSeparator1,
            this.btnRun,
            this.btnDebug,
            this.btnStop,
            this.btnContinue,
            this.btnStepIn,
            this.btnStepOver,
            this.btnStepOut});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(257, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 1;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.Controls.Add(this.toolStrip1, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.tvwLocals, 0, 1);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 2;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(257, 256);
            this.tableLayoutPanel.TabIndex = 2;
            // 
            // tvwLocals
            // 
            this.tvwLocals.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvwLocals.Location = new System.Drawing.Point(3, 28);
            this.tvwLocals.Name = "tvwLocals";
            this.tvwLocals.Size = new System.Drawing.Size(251, 225);
            this.tvwLocals.TabIndex = 2;
            // 
            // btnBreak
            // 
            this.btnBreak.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnBreak.Enabled = false;
            this.btnBreak.Image = global::PythonDebugger.Properties.Resources.control_record;
            this.btnBreak.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnBreak.Name = "btnBreak";
            this.btnBreak.Size = new System.Drawing.Size(23, 22);
            this.btnBreak.Text = "toolStripButton1";
            this.btnBreak.Click += new System.EventHandler(this.btnBreak_Click);
            // 
            // btnRun
            // 
            this.btnRun.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRun.Image = global::PythonDebugger.Properties.Resources.control;
            this.btnRun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(23, 22);
            this.btnRun.Text = "toolStripButton2";
            this.btnRun.ToolTipText = "Run";
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // btnDebug
            // 
            this.btnDebug.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDebug.Image = global::PythonDebugger.Properties.Resources.bug;
            this.btnDebug.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDebug.Name = "btnDebug";
            this.btnDebug.Size = new System.Drawing.Size(23, 22);
            this.btnDebug.Text = "toolStripButton3";
            this.btnDebug.ToolTipText = "Debug";
            this.btnDebug.Click += new System.EventHandler(this.btnDebug_Click);
            // 
            // btnStop
            // 
            this.btnStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnStop.Image = global::PythonDebugger.Properties.Resources.control_stop_square;
            this.btnStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(23, 22);
            this.btnStop.Text = "toolStripButton4";
            this.btnStop.ToolTipText = "Stop";
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnStepIn
            // 
            this.btnStepIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnStepIn.Image = global::PythonDebugger.Properties.Resources.arrow_step;
            this.btnStepIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStepIn.Name = "btnStepIn";
            this.btnStepIn.Size = new System.Drawing.Size(23, 22);
            this.btnStepIn.Text = "toolStripButton6";
            this.btnStepIn.ToolTipText = "Step In";
            this.btnStepIn.Click += new System.EventHandler(this.btnStepIn_Click);
            // 
            // btnStepOver
            // 
            this.btnStepOver.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnStepOver.Image = global::PythonDebugger.Properties.Resources.arrow_step_over;
            this.btnStepOver.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStepOver.Name = "btnStepOver";
            this.btnStepOver.Size = new System.Drawing.Size(23, 22);
            this.btnStepOver.Text = "toolStripButton5";
            this.btnStepOver.ToolTipText = "Step Over";
            this.btnStepOver.Click += new System.EventHandler(this.btnStepOver_Click);
            // 
            // btnStepOut
            // 
            this.btnStepOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnStepOut.Enabled = false;
            this.btnStepOut.Image = global::PythonDebugger.Properties.Resources.arrow_step_out;
            this.btnStepOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStepOut.Name = "btnStepOut";
            this.btnStepOut.Size = new System.Drawing.Size(23, 22);
            this.btnStepOut.Text = "toolStripButton7";
            this.btnStepOut.ToolTipText = "Step Out";
            this.btnStepOut.Click += new System.EventHandler(this.btnStepOut_Click);
            // 
            // btnContinue
            // 
            this.btnContinue.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnContinue.Image = global::PythonDebugger.Properties.Resources.arrow;
            this.btnContinue.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.Size = new System.Drawing.Size(23, 22);
            this.btnContinue.Text = "toolStripButton1";
            this.btnContinue.ToolTipText = "Continue";
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            // 
            // frmPythonDebug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(257, 256);
            this.Controls.Add(this.tableLayoutPanel);
            this.KeyPreview = true;
            this.Name = "frmPythonDebug";
            this.Text = "Python Debug";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.ToolStripButton btnBreak;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnRun;
        private System.Windows.Forms.ToolStripButton btnDebug;
        private System.Windows.Forms.ToolStripButton btnStop;
        private System.Windows.Forms.ToolStripButton btnStepOver;
        private System.Windows.Forms.ToolStripButton btnStepIn;
        private System.Windows.Forms.ToolStripButton btnStepOut;
        private System.Windows.Forms.TreeView tvwLocals;
        private System.Windows.Forms.ToolStripButton btnContinue;
    }
}