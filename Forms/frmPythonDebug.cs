﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PythonDebugger
{
    public partial class frmPythonDebug : Form
    {
        Process p = null;
        bool running = false;

        PythonDebugClient client = new PythonDebugClient();
        PythonSocketRequest response;

        int debugCurLineMarker;
        int curLine;

        public frmPythonDebug()
        {
            InitializeComponent();

            SetDialogState(false, false);
        }

        private void RunPython()
        {
            ProcessStartInfo info = new ProcessStartInfo();
            //info.Arguments = ???;
            //info.WorkingDirectory = ???;
            info.FileName = "python"; //TODO: Which python? Get from registry?

            StartPython(info);
        }

        private void DebugPython()
        {
            Debug.WriteLine(Main.GetCurrentFilename());

            ProcessStartInfo info = new ProcessStartInfo();
            info.UseShellExecute = false;
            info.Arguments = "debug.py";
            info.WorkingDirectory = $"{Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)}\\PythonDebugger\\";
            info.FileName = "python"; //TODO: Which python? Get from registry?
            //info.CreateNoWindow = true;

            StartPython(info);

            //Prepare current line marker
            debugCurLineMarker = Main.DefineMarker(Color.Yellow);

            //Get current filename
            string filename = Main.GetCurrentFilename();

            client.StartClient(filename);
            response = client.Receive();

            if (response.response == "break" && response.line > 0)
            {
                ShowLocals(response.locals);

                curLine = response.line - 1;
                Main.AddMarkerToLine(curLine, debugCurLineMarker);
            }
            //client.StopClient();
        }

        public void StartPython(ProcessStartInfo startInfo)
        {
            if (running)
                return;

            try
            {
                p = new Process();
                p.StartInfo = startInfo;

                //p.StartInfo.RedirectStandardInput = true;
                //p.StartInfo.RedirectStandardError = true;
                //p.StartInfo.RedirectStandardOutput = true;
                //p.StartInfo.StandardOutputEncoding = System.Text.Encoding.UTF8;
                //p.EnableRaisingEvents = true;
                //p.StartInfo.UseShellExecute = false;
                //p.StartInfo.CreateNoWindow = true;
                //p.StartInfo.FileName = lispImpl;

                p.Exited += p_Exited;

                p.Start();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public void StopPython()
        {
            tvwLocals.Nodes.Clear();

            Main.RemoveMarkerFromAllLines(debugCurLineMarker);
            Main.DeleteMarker(debugCurLineMarker);

            if (client.sock.Connected)
                client.StopClient();

            if (running && !p.HasExited)
                p.Kill();
        }

        private void SetDialogState(bool isRunning, bool isDebugging)
        {
            btnBreak.Visible = !isRunning;
            btnRun.Visible = !isRunning;
            btnDebug.Visible = !isRunning;

            btnStop.Visible = isRunning;
            btnContinue.Visible = isRunning && isDebugging;
            btnStepIn.Visible = isRunning && isDebugging;
            btnStepOver.Visible = isRunning && isDebugging;
            btnStepOut.Visible = isRunning && isDebugging;
        }

        #region Process Events and Callbacks

        void p_Exited(object sender, EventArgs e)
        {
            running = false;
            //txtConsole.Invoke(new Action(() => txtConsole.Clear()));

            //TODO: Clear dialog...
        }

        #endregion

        #region Toolbar Button Events

        private void btnBreak_Click(object sender, EventArgs e)
        {

        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            RunPython();
            SetDialogState(true, false);
        }

        private void btnDebug_Click(object sender, EventArgs e)
        {
            DebugPython();
            SetDialogState(true, true);
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            StopPython();
            SetDialogState(false, false);
        }

        private void btnStepIn_Click(object sender, EventArgs e)
        {
            HandleStepRequest("step");
        }

        private void btnStepOver_Click(object sender, EventArgs e)
        {
            HandleStepRequest("next");
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            HandleStepRequest("continue");
        }

        private void btnStepOut_Click(object sender, EventArgs e)
        {
            //HandleStepRequest("out");
        }

        #endregion

        #region Methods

        private void HandleStepRequest(string stepRequest)
        {
            client.Send(new PythonSocketRequest { request = stepRequest });
            PythonSocketRequest response = client.Receive();
            
            if (response.response == "complete")
            {
                tvwLocals.Nodes.Clear();
                Main.RemoveMarkerFromAllLines(debugCurLineMarker);
                Main.DeleteMarker(debugCurLineMarker);
                client.StopClient(); //TODO: Send disconnect request...
                SetDialogState(false, false);
            }
            else if (response.response == "break" && response.line > 0)
            {
                ShowLocals(response.locals);

                Main.RemoveMarkerFromLine(curLine, debugCurLineMarker);

                curLine = response.line - 1;
                Main.AddMarkerToLine(curLine, debugCurLineMarker);
            }
        }

        private void ShowLocals(object locals, TreeNode node = null)
        {
            if (node == null)
            {
                tvwLocals.Nodes.Clear();
                TreeNode localsNode = tvwLocals.Nodes.Add("Locals");
                ShowLocals(locals, localsNode);

                tvwLocals.ExpandAll();
            }
            else
            {
                if (locals.GetType().Name.ToLower().Contains("[]"))
                {
                    Array localsArr = locals as Array;
                    TreeNode arrNode = node.Nodes.Add(locals.GetType().Name);
                    foreach (var item in localsArr)
                    {
                        ShowLocals(item, arrNode);
                    }
                }
                else if (locals.GetType().Name.ToLower().Contains("dictionary"))
                {
                    Dictionary<string, object> localsDict = locals as Dictionary<string, object>;
                    foreach (string key in localsDict.Keys)
                    {
                        TreeNode dictNode = node.Nodes.Add(key);
                        ShowLocals(localsDict[key], dictNode);
                    }
                }
                else
                {
                    TreeNode arrNode = node.Nodes.Add($"{locals.GetType().Name}: {locals}");
                }
            }
        }

        #endregion
    }
}
